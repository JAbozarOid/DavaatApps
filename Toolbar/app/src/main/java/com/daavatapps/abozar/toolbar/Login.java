package com.daavatapps.abozar.toolbar;


import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;

import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Abozar on 12/14/2016.
 */

public class Login extends AppCompatActivity  {

    ImageButton ib_TakePhoto;
    TextView    tv_TextPhoto;
    Intent intentloadImage;
    Intent intentGoToMainPage;
    Bitmap bitmap;
    final static int Data = 0; // Data = Request Code
    EditText et_Username , et_Password;
    EditText et_EmailAddress;
    Button btn_Enter , btn_Register;
    String sendUsername,sendEmailAddress,sendPassword;


    /*public static String FILE_NAME = "mySharedPrefes";
    SharedPreferences DataSharedPrefes;*/



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        init();


        //DataSharedPrefes = getSharedPreferences("KEYSHARED" , 0);

        ib_TakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intentloadImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentloadImage , 0);

            }
        });

        btn_Enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendUsername = et_Username.getText().toString();
                sendEmailAddress = et_EmailAddress.getText().toString();
                sendPassword = et_Password.getText().toString();

                if (sendUsername.isEmpty() || sendEmailAddress.isEmpty() || sendPassword.isEmpty()){
                    Toast.makeText(getApplicationContext(), "لطفا فیلدهای مربوطه را کامل بفرمایید" , Toast.LENGTH_LONG).show();
                }



                else {

                    Database db = new Database(Login.this);
                    try {
                        db.open();

                        String storedPassword = db.getSinlgeEntry(sendUsername);
                        if (sendPassword.equals(storedPassword)) {
                            Toast.makeText(Login.this, "نام کاربری با رمز عبور مطابقت دارد", Toast.LENGTH_LONG).show();

                            validateFields(sendUsername,sendEmailAddress,sendPassword);


                        } else {
                            Toast.makeText(Login.this, "نام کاربری وارد شده با رمز عبور مطابقت ندارد", Toast.LENGTH_LONG).show();
                        }

                        db.close();
                    } catch (Exception e) {
                        String err = e.toString();
                        Toast.makeText(getApplicationContext(), err, Toast.LENGTH_LONG).show();
                    }


                }
            }
        });

        btn_Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGoToRegister = new Intent(Login.this , Register.class);
                startActivity(intentGoToRegister);

            }
        });


    }

    private void init() {
        ib_TakePhoto = (ImageButton) findViewById(R.id.ibTakePhoto);
        tv_TextPhoto = (TextView) findViewById(R.id.tvTextPhoto);
        et_Username = (EditText) findViewById(R.id.etUsername);
        et_EmailAddress  = (EditText) findViewById(R.id.etEmailAddress);
        et_Password = (EditText) findViewById(R.id.etPassword);
        btn_Enter = (Button) findViewById(R.id.btnEnter);
        btn_Register = (Button) findViewById(R.id.btnRegister);
    }

    public void TextChanged(View v){

        et_Username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               // Toast.makeText(getBaseContext() , s  , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void validateFields(String uName , String eMail , String pass){
 /*       else {
            *//*SharedPreferences.Editor editor = DataSharedPrefes.edit();
            editor.putString("KEYSHARED" , sendUsername);
            editor.commit();
            Toast.makeText(getApplicationContext() , "نام کاربری شما با موفقیت ثبت گردید" , Toast.LENGTH_SHORT).show();*//*
        }*/

        String validEmailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if ((uName.length()<=6)) {

            et_Username.setError(" نام کاربری باید حداقل 6 کاراکتر باشد");
            //Toast.makeText(getBaseContext() , "لطفا فیلدهای مربوطه را کامل بفرماید" , Toast.LENGTH_LONG).show();
        }
      /* else if (!eMail.equals(validEmailPattern)) {
            et_EmailAddress.setError("آدرس ایمیل معتبر نمی باشد");
        }*/

        else if (pass.length() < 6) {
                    et_Password.setError("رمز عبور حداقل باید 6 رقم باشد");
                }
        else{
                    goToMainPage();
                }
    }




    public void goToMainPage(){

        sendUsername = et_Username.getText().toString();
        sendEmailAddress = et_EmailAddress.getText().toString();

        Bundle bundleSendEmail = new Bundle();
        bundleSendEmail.putString("EMAIL", sendEmailAddress);

        Bundle bundleSendUname = new Bundle();
        bundleSendUname.putString("USERNAME", sendUsername);

        intentGoToMainPage = new Intent(Login.this, MainActivity.class);
        intentGoToMainPage.putExtras(bundleSendUname);
        intentGoToMainPage.putExtras(bundleSendEmail);
        startActivity(intentGoToMainPage);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK){

            Bundle bundleLoadImage = data.getExtras();
            bitmap = (Bitmap) bundleLoadImage.get("Data");
            ib_TakePhoto.setImageBitmap(bitmap);

        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
