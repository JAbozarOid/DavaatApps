package com.daavatapps.abozar.toolbar;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Abozar on 12/17/2016.
 */

public class Database {

    //database name and version
    private static final String DATABASE_NAME = "davaatApps";
    private static final int DATABASE_VERSION = 1;

    //Table name
    private static final String DATABASE_TABLE = "Register";

    // table fields
    public static final String KEY_Id = "_id";
    public static final String KEY_FirstName = "_FirstName";
    public static final String KEY_LastName = "_LastName";
    public static final String KEY_NickName = "_NickName";
    public static final String KEY_Password = "_Password";
    public static final String KEY_ConfirmPassword = "_ConfirmPassword";
    public static final String KEY_Email = "_Email";
    public static final String KEY_MobileNumber = "_MobileNumber";

    private DBHelper myHelper;
    private final Context myContext;
    private SQLiteDatabase davaatApps;

    private static class DBHelper extends SQLiteOpenHelper {

        // constructor of DBHelper
        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            // mikhaim ye database besazim be hamrahe table va field haye ke mikhaim
            // dasturate sql ba hoorufe BOZORG
            db.execSQL("CREATE TABLE " + DATABASE_TABLE + " (" +
                    KEY_Id + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                    KEY_FirstName + " TEXT NOT NULL , " +
                    KEY_LastName + " TEXT NOT NULL , " +
                    KEY_NickName + " TEXT NOT NULL , " +
                    KEY_Password + " INTEGER , " +
                    KEY_ConfirmPassword + " INTEGER , " +
                    KEY_Email + " TEXT NOT NULL , " +
                    KEY_MobileNumber + " TEXT NOT NULL);"
            );

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE); // age table ba in esm vujud dasht hazfesh kon
            onCreate(db);
        }
    }

    // constructor of Database
    public Database(Context context) {

        myContext = context;
    }

    // inja database khod ra baz mikonim
    public Database open() throws SQLException {
        myHelper = new DBHelper(myContext);
        davaatApps = myHelper.getWritableDatabase();
        return this;
    }

    // bastane database
    public void close() {
        myHelper.close();
    }


    public String getSinlgeEntry(String userName) {


        Cursor cursor=davaatApps.query("Register", null, " _NickName=?", new String[]{userName}, null, null, null);
        if(cursor.getCount()<1) // NickName Not Exist
            return "NOT EXIST";
        cursor.moveToFirst();
        String password= cursor.getString(cursor.getColumnIndex("_Password"));
        return password;


    }





    public long makeEntry(String FName, String LName, String NName, String pass, String cPass, String email, String mobile) {

        ContentValues cv = new ContentValues();
        cv.put(KEY_FirstName, FName);
        cv.put(KEY_LastName, LName);
        cv.put(KEY_NickName, NName);
        cv.put(KEY_Password, pass);
        cv.put(KEY_ConfirmPassword, cPass);
        cv.put(KEY_Email, email);
        cv.put(KEY_MobileNumber, mobile);

        //String sql = "(KEY_FirstName,KEY_LastName,KEY_NickName,KEY_Password,KEY_ConfirmPassword,KEY_Email,KEY_MobileNumber) VALUES ('" + FName + "','" + LName + "','" + NName + "','" + pass + "','" + cPass + "','" + email + "','" + mobile + "')";
        //davaatApps.execSQL( "INSERT INTO" + DATABASE_TABLE + sql );

        // dar in marhale bayad cv hae ke vared kardim o berizim tu database
        return davaatApps.insert(DATABASE_TABLE , null , cv);

    }

    public String getDataFromDB(){

        String result = "";
        String[] columns = new String[]{KEY_Id , KEY_FirstName , KEY_LastName , KEY_NickName , KEY_Password , KEY_ConfirmPassword , KEY_Email , KEY_MobileNumber};

        Cursor cursor = davaatApps.query(DATABASE_TABLE , columns , null ,null ,null ,null ,null);

        int ROW_ID = cursor.getColumnIndex(KEY_Id);
        int ROW_FName = cursor.getColumnIndex(KEY_FirstName);
        int ROW_LName = cursor.getColumnIndex(KEY_LastName);
        int ROW_NName = cursor.getColumnIndex(KEY_NickName);
        int ROW_Pass = cursor.getColumnIndex(KEY_Password);
        int ROW_cPass = cursor.getColumnIndex(KEY_ConfirmPassword);
        int ROW_Email = cursor.getColumnIndex(KEY_Email);
        int ROW_Mobile = cursor.getColumnIndex(KEY_MobileNumber);

        for(cursor.moveToFirst() ; !cursor.isAfterLast() ; cursor.moveToNext()){

            result += cursor.getString(ROW_ID) + " " + cursor.getString(ROW_FName) + " " + cursor.getString(ROW_LName) + " " + cursor.getString(ROW_NName) + " " + cursor.getString(ROW_Pass) + " " + cursor.getString(ROW_cPass) + " " + cursor.getString(ROW_Email) + " "+ cursor.getString(ROW_Mobile) + "\n";

        }

        return result;
    }


}
